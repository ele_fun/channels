package foo

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type (
	f   struct{}
	Bla struct {
		one   string
		two   string
		three string
		err   error
	}
)

var Foo f

func (b Bla) String() string {
	if b.err != nil {
		return fmt.Sprint("error: ", b.err)
	}
	return fmt.Sprintf("one: %s, two: %s, three: %s", b.one, b.two, b.three)
}

// Bar - este método ejecuta 3 gorotines
// donde tiene que pasar información de unas variables  mediantes
// canales
func (fn *f) Bar() {
	var (
		oneChan   = make(chan string)
		twoChan   = make(chan string)
		threeChan = make(chan string)
		errChan   = make(chan error)
	)

	go bla(oneChan, errChan)
	go bla(twoChan, errChan)
	go bla(threeChan, errChan)

	fmt.Println(blaSelect(oneChan, twoChan, threeChan, errChan))
}
func blaSelect(oneChan, twoChan, threeChan chan string, errChan chan error) (b Bla) {

	var counter int
	for {
		select {
		case o := <-oneChan:
			fmt.Println("one: ", o)
			b.one = o
			counter++
		case t := <-twoChan:
			fmt.Println("two: ", t)
			b.two = t
			counter++
		case th := <-threeChan:
			fmt.Println("three :", th)
			b.three = th
			counter++
		case e := <-errChan:
			fmt.Println("err: ", e)
			counter = 3
			b.err = e
			return
		default:
			if counter == 3 {
				return
			}
		}
	}
}

func bla(c chan<- string, eC chan<- error) {
	random := rand.Intn(10)
	time.Sleep(time.Duration(random) * time.Second)

	if random%7 == 0 {
		eC <- errors.New("err, multiple 7")
	}
	c <- fmt.Sprint(random)
}
