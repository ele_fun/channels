package main

import (
	"fmt"

	"gitlab.com/Elevarup/ele_fun/channels/internal/foo"
)

func main() {
	fmt.Println("init")
	foo.Foo.Bar()
}
